MATERIAL {
  ID               26               ; ID of the material to replace [0 - 26]
  Name             "GRAVEL"     ; Display name

  Spark            true             ; Material emits particles
  Skid             true             ; Skid marks appear on material
  OutOfBounds      false            ; Not implemented
  Corrugated       true             ; Material is bumpy
  Moves            false            ; Moves cars like museum conveyors
  Dusty            true             ; Material emits dust

  Roughness        1.0              ; Roughness of the material
  Grip             0.8              ; Grip of the material
  Hardness         0.8              ; Hardness of the material

  SkidSound        105               ; Sound when skidding [0:Normal, 1:Rough]
  ScrapeSound      5                ; Car body scrape [0:Normal]

  SkidColor        207 215 220      ; Color of the skid marks
  CorrugationType  2                ; Type of bumpiness [0 - 7]
  DustType         2                ; Type of dust
  Velocity         0.0 0.0 0.0      ; Speed and direction cars are moved in
}

CORRUGATION {
  ID              2                             ; Corrugation to replace [0-7]
  Name            "GRAVEL"                        ; Display name

  Amplitude       2.000000                      ; Amplitude of bumps (strength)
  Wavelength      0.500000 0.500000             ; Frequency of bumps
}

DUST {
  ID              2                             ; Dust to replace [0 - 5]
  Name            "GRAVEL"                        ; Display name

  SparkType       5                             ; Particle to emit [0 - 30]
  ParticleChance  1.000000                      ; Probability of a particle
  ParticleRandom  0.400000                      ; Probability variance

  SmokeType       3                            ; Smoke particle to emit [0-30]
  SmokeChance     0.900000                      ; Probability of a smoke part.
  SmokeRandom     0.600000                      ; Probability variance
}

SPARK {
  ID              3                            ; Particle to replace [0 - 30]
  Name            "GRAVEL PARTICLES"                    ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      false                         ; Collision with camera
  HasTrail        false                         ; Particle has a trail
  FieldAffect     false                         ; Is affected by force fields
  Spins           true                          ; Particle spins
  Grows           true                          ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            6.000000 6.000000             ; Size of the particle
  UV              0.000000 0.000000             ; Top left UV coordinates
  UVSize          0.250000 0.250000             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           30 26 14                      ; Color of the particle

  Mass            0.030000                      ; Mass of the particle
  Resistance      0.002000                      ; Air resistance
  Friction        0.000000                      ; Sliding friction
  Restitution     0.000000                      ; Bounciness

  LifeTime        0.500000                      ; Maximum life time
  LifeTimeVar     0.100000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     6.000000                      ; Variation of the spin rate

  SizeVar         2.000000                      ; Size variation
  GrowRate        60.000000                     ; How quickly it grows
  GrowRateVar     20.000000                     ; Grow variation

  TrailType       -1                            ; ID of the trail to use
}

MATERIAL {
  ID              1                             ; Material to replace [0 - 26]
  Name            "STONE"                      ; Display name

  Skid            true                          ; Skidmarks appear on material
  Spark           false                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      true                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           false                         ; Material emits dust

  Roughness       1.000000                      ; Roughness of the material
  Grip            1.000000                      ; Grip of the material
  Hardness        1.000000                     ; Hardness of the material

  SkidSound       104                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       112 112 112                   ; Color of the skidmarks
  CorrugationType 1                             ; Type of bumpiness [0 - 7]
  DustType        1                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

CORRUGATION {
  ID              1                             ; Corrugation to replace [0-7]
  Name            "STONE"                     ; Display name

  Amplitude       1.300000                      ; Amplitude of bumps (strength)
  Wavelength      10.000000 10.000000           ; Frequency of bumps
}

DUST {
  ID              1                             ; Dust to replace [0 - 5]
  Name            "STONE"                      ; Display name

  SparkType       10                             ; Particle to emit [0 - 30]
  ParticleChance  0.030000                      ; Probability of a particle
  ParticleRandom  0.030000                      ; Probability variance

  SmokeType       10                            ; Smoke particle to emit [0-30]
  SmokeChance     0.010000                      ; Probability of a smoke part.
  SmokeRandom     0.020000                      ; Probability variance
}

MATERIAL {
  ID              20                             ; Material to replace [0 - 26]
  Name            "HARD STONE"                      ; Display name

  Skid            true                          ; Skidmarks appear on material
  Spark           false                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      true                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           false                         ; Material emits dust

  Roughness       0.800000                      ; Roughness of the material
  Grip            1.000000                      ; Grip of the material
  Hardness        0.800000                     ; Hardness of the material

  SkidSound       104                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       112 112 112                   ; Color of the skidmarks
  CorrugationType 3                             ; Type of bumpiness [0 - 7]
  DustType        1                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

CORRUGATION {
  ID              3                             ; Corrugation to replace [0-7]
  Name            "HARD STONE"                     ; Display name

  Amplitude       2.000000                      ; Amplitude of bumps (strength)
  Wavelength      10.000000 10.000000           ; Frequency of bumps
}

MATERIAL {
  ID              19                             ; Material to replace [0 - 26]
  Name            "WHITE STONE"                      ; Display name

  Skid            true                          ; Skidmarks appear on material
  Spark           false                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      false                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           true                         ; Material emits dust

  Roughness       1.000000                      ; Roughness of the material
  Grip            1.000000                      ; Grip of the material
  Hardness        1.000000                     ; Hardness of the material

  SkidSound       104                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       112 112 112                   ; Color of the skidmarks
  CorrugationType 1                             ; Type of bumpiness [0 - 7]
  DustType        1                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

MATERIAL {
  ID              18                             ; Material to replace [0 - 26]
  Name            "GREEN STONE"                      ; Display name

  Skid            true                          ; Skidmarks appear on material
  Spark           false                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      false                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           true                         ; Material emits dust

  Roughness       1.000000                      ; Roughness of the material
  Grip            1.000000                      ; Grip of the material
  Hardness        1.000000                     ; Hardness of the material

  SkidSound       104                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       112 112 112                   ; Color of the skidmarks
  CorrugationType 1                             ; Type of bumpiness [0 - 7]
  DustType        4                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

DUST {
  ID              4                             ; Dust to replace [0 - 5]
  Name            "GREEN STONE DUST"                        ; Display name

  SparkType       0                             ; Particle to emit [0 - 30]
  ParticleChance  0.400000                      ; Probability of a particle
  ParticleRandom  0.800000                      ; Probability variance

  SmokeType       28                            ; Smoke particle to emit [0-30]
  SmokeChance     0.100000                      ; Probability of a smoke part.
  SmokeRandom     0.400000                      ; Probability variance
}

SPARK {
  ID              28                            ; Particle to replace [0 - 30]
  Name            "GREEN STONE DUST"                    ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      false                         ; Collision with camera
  HasTrail        false                         ; Particle has a trail
  FieldAffect     false                         ; Is affected by force fields
  Spins           true                          ; Particle spins
  Grows           true                          ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            4.000000 4.000000             ; Size of the particle
  UV              0.000000 0.000000             ; Top left UV coordinates
  UVSize          0.250000 0.250000             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           50 50 28                      ; Color of the particle

  Mass            0.030000                      ; Mass of the particle
  Resistance      0.002000                      ; Air resistance
  Friction        0.000000                      ; Sliding friction
  Restitution     0.000000                      ; Bounciness

  LifeTime        0.500000                      ; Maximum life time
  LifeTimeVar     0.100000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     6.000000                      ; Variation of the spin rate

  SizeVar         2.000000                      ; Size variation
  GrowRate        120.000000                      ; How quickly it grows
  GrowRateVar     20.000000                     ; Grow variation

  TrailType       -1                            ; ID of the trail to use
}

;==============================================================================
;  PICKUPS
;==============================================================================

PICKUPS {
  SpawnCount      5 2                           ; Initial and per-player count
  EnvColor        255 255 128                   ; Color of shininess (RGB)
  LightColor      170 64 0                      ; Color of light (RGB)
}

}
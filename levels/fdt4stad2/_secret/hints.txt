1) The password is composed of 16 characters all in caps. 
2) There is a total of 8 hints that you need to find to get the password.
3) Each hint will reveal 2 characters of the password.
4) The first 4 hints can be found in "RC Stadium 1" and the last 4 in "RC Stadium 2".
5) The reward is nothing spectacular, so please lower your expectations xD.

Good luck!

1/8: LE
2/8: KS
3/8: 5P
4/8: T9
5/8: IL
6/8: TH
7/8: 25
8/8: RJ
 
 Swan Street - July 2009
 -----------------------
 by hilaire9
 -----------
 Length: 542 meters                                      
 Type: Extreme
 ====================================================
 * To Install: Extract into your main Re-volt folder.
 =========================================================
 Swan Street run along a series of canals. It is named for 
 the swans you can sometimes see swimming in the canals.
 -------------------------------------------------------------
 Tools: 3D Studio Max 8, ASE Tools, and MAKEITGOOD edit modes.   
 -------------------------------------------------------------
 --- hilaire9's Home Page: http://members.cox.net/alanzia


07/06/2017 SebR add reversed way
